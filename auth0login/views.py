from django.http import HttpRequest
from django.shortcuts import render


def login(request):
    return render(request, "login.html", {})


def oauth_redirect(request):
    return render(request, "oauth-redirect.html", {})
